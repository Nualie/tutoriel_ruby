# Les chaînes de caractères et expressions régulières

Comme dit précédemment, les chaînes de caractères sont réservés au traitement des fichiers et à la communication avec un utilisateur. Vous devez le plus possible éviter de vous servir de celles-ci pour faire fonctionner votre code (tableau associatif chaîne => valeur par exemple).

En Ruby, les chaînes de caractères sont des String, les expressions régulières sont des Regexp.

Une expression régulière se définit de deux manières différentes en ruby :
* `/<expression>/<flags>` la manière commune où `<expression>` est l'expression régulière et `<flags>` sont les flags optionnels de l'expression.
* `%r{<expression>}<flags>` la manière alternative où `<expression>` est l'expression régulière (pouvant contenir `/` sans le précéder de `\`) et `<flags>` sont les flags optionnels de l'expression.

Les expressions régulières permettent de réaliser des traitements sur les chaînes de caractères. On s'en servira donc beaucoup pour certains types d'applications.

## Manipuler les chaînes de caractères

On peut réaliser plusieurs types de manipulations sur les chaînes de caractères, nous allons voir les plus communes et plus intéressantes.

### Concaténer des chaînes

Deux manières de concaténer des chaînes de caractères : 
* `chaîne1 + chaîne2` : concatène les deux chaînes dans une nouvelle chaîne.
* `chaîne1 << chaîne2` ou `chaîne1.concat(chaîne2)` : concatène chaîne2 dans chaîne1.

Exemples :
```ruby
str1 = 'Hello '
str2 = 'world!'
str1 + str2 # "Hello world!"
str1 << str2 # "Hello world!"
# str1 = "Hello world!"
```

### Concaténer une chaîne au début de la chaîne de caractères

Si vous voulez ajouter du contenu au début de la chaîne de caractères, vous pouvez utiliser la méthode `prepend`.

Exemple :
```ruby
str = 'world!'
str.prepend('Hello ')
# str = "Hello world!"
```

### Changer la case d'une chaîne

Il est possible de changer la case d'une chaîne à l'aide de deux méthodes :
* `downcase` : Met tout en minuscule.
* `upcase` : Met tout en majuscule.

### Comparer des chaînes en ignorant la casse

Pour comparer deux chaînes en ignorant la casse on utilise `casecmp?`.

Exemple :
```ruby
str = "HeLlO wOrLd!"
str.casecmp?('Hello world!') # true
str == 'Hello world!' # false # C'est la comparaison basique
```

### Détecter une chaîne au début ou à la fin

Si vous voulez savoir si une chaîne commence ou termine par une autre chaîne vous avez deux méthodes pour ça :

* `start_with?` si la chaîne commence par le paramètre (chaîne ou expression régulière).
* `end_with?` si la chaîne termine par le paramètre (chaîne ou expression régulière).

Exemples :
```ruby
str = 'Chaine de test.'
str.start_with?('Chaine') # true
str.start_with?(/chaine/i) # true
str.end_with?('.') # true
str.end_with?(/test\./i) # true
```

### Aligner le contenu d'une chaîne

Trois façons d'aligner le contenu d'une chaîne :
* `center` pour centrer le contenu
* `ljust` pour aligner à gauche
* `rjust` pour aligner à droite

Exemples :
```ruby
str = 'test'
str.center(10) # "   test   "
str.center(10, '=') # "===test==="
str.ljust(10) # "test      "
str.ljust(10, '=') # "test======"
str.rjust(10) # "      test"
str.rjust(10, '=') # "======test"
```

### Nettoyer une chaîne

Plusieurs méthodes de nettoyage selon vos souhaits.

* `chomp` : Supprime le saut de ligne à la fin de la chaîne (entrée utilisateur).
* `strip` : Supprime tout ce qui s'apparente à des espaces, saut de ligne, tabulation à droite et à gauche de la chaîne.
* `rstrip` : Version de `strip` mais que à droite.
* `lstrip` : Version de `strip` mais que à gauche.

Exemples :
```ruby
str = "   test  \n"
str.chomp # "   test  "
str.strip # "test"
str.lstrip # "test  \n"
str.rstrip # "   test"
```

### Lire des caractères dans la chaîne

Ceci se fait comme pour les tableaux, à l'aide de l'opérateur `[]`.

Exemples :
```ruby
str = 'Hello'
str[1] # "e"
str[1, 2] # "el"
str[1..3] # "ell"
str[2..-1] # "llo"
```

### Modifier une portion de la chaîne

Comme pour les tableaux l'opérateur `[]=` permet de modifier une portion. Le fonctionnement est identique à l'exception qu'on manipule des chaînes de caractères.

Exemple :
```ruby
str = 'H...O'
str[1, 3] = 'ell'
# str = "HellO"
str[-1] = 'o'
# str = "Hello"
```

### Obtenir la taille de la chaîne

Les chaînes de caractères ont deux tailles :
* `size` leur nombre de caractères
* `bytesize` leur nombre d'octets

Selon comment vous traitez votre chaîne l'une ou l'autre méthode peut vous servir.

Exemple :
```ruby
str = "ça"
str.size # 2
str.bytesize # 3
```

### Lire ou modifier les octets d'une chaîne

Lorsque vous vous intéressez aux octets de la chaîne plus que ses caractères, vous pouvez utiliser les deux méthodes suivantes :
* `setbyte` pour modifier un octet dans la chaîne.
* `getbyte` pour lire un octet dans la chaîne.

Exemple :
```ruby
str = 'HEllo'
str.getbyte(1) # 69
str.setbyte(1, 101)
# str = "Hello"
```

### Remplacer des éléments d'une chaîne par d'autres

Il existe plusieurs méthodes pour remplacer des éléments d'une chaîne par d'autres :
* `tr` remplace les caractères donnés en premier paramètre par les caractères associés donnés en deuxième paramètre dans la chaîne.
* `sub` remplace le pattern (premier paramètre) par le deuxième paramètre ou le résultat du bloc exécuté.
* `gsub` remplace tous les pattern détectés (premier paramètre) par le deuxième paramètre ou le résultat du bloc.

Exemples :
```ruby
str = 'Hello world!'
str.tr('l', '1') # "He11o wor1d!"
str.tr('lo','10') # "He110 w0r1d!"
str.tr('elor', '*') # "H**** w***d!"
str.sub('l', '1') # "He1lo world!"
str.sub(/[a-z]+/, '') # "H world!"
str.sub(/( |!)/) do |full_match|
  next('[space]') if $1 == ' '
  next('(!)')
end # "Hello[space]world!"
str.gsub('l', '1') # "He11o wor1d!"
str.gsub(/[a-z]+/, '') # "H !"
str.gsub(/( |!)/) do |full_match|
  next('[space]') if $1 == ' '
  next('(!)')
end # "Hello[space]world(!)"
```

### Détecter des pattern dans une chaîne

Deux opérateurs permettent de détecter la présence d'un pattern dans une chaîne et une méthode permet d'obtenir plus de précision.

* `=~` Renvoie la position où le pattern a été détecté, renvoie `nil` si le pattern n'est pas présent.
* `!~` Indique si le pattern ne se trouve pas dans la chaîne (`true`), renvoie `false` sinon.
* `match` Renvoie les résultats de correspondance de la chaîne avec l'expression régulière.

Exemples :
```ruby
str = 'Hello world!'
str =~ /e/ # 1
str =~ /z/ # nil
str !~ /z/ # true
str !~ /e/ # false
str.match(/(l|e)+/) # #<MatchData "ell" 1:"l">
```

## Formater des chaînes.

Voilà une partie bien importante. En Ruby on peut formater des chaînes, il existe deux fonctions pour ça : `sprintf` et `format`. Les deux sont des alias. Il est recommandé d'utiliser `format` plutôt que `sprintf` car nous aurons tendance à nommer les parties formatées.

La méthode `format` est une fonction globale, elle prend en premier paramètre la chaîne de format et les autres paramètres sont les remplacements.

Exemple :
```ruby
format('Vous avez %<age>d ans.', age: 18) # "Vous avez 18 ans."
```

Dans cette chaîne, `%` annonce que nous allons attendre une valeur à formater, `<age>` indique le nom de cette valeur `d` indique le format de cette valeur (nombre décimal).

De manière générale à chaque fois qu'une valeur doit être formatée nous allons écrire :
`%<[nom]>[flag][taille][.precision][type]`
Où :
* `[nom]` doit être remplacé par le nom utilisé pour identifier la valeur.
* `[flag]` (optionnel) peut être remplacé par :

  * espace : remplacer tous les caractères non occupé par le chiffre par des espaces.
  * `0` : remplacer tous les caractères non occupé par le chiffre par des 0.
  * `#` : Afficher le 0x / 0b / 0 pour les bases hexadécimale, binaire et octale.
  * `+` : Afficher un + pour les nombres positifs au début du nombre.
  * `-` : Justifier le nombre à gauche au lieu d'à droite.
* `[taille]` (optionnel) indique la taille que vous désirez utiliser en nombre de caractères pour la valeur formatée.
* `[.precision]` (optionnel) indique :

  * Le nombre de chiffres après la virgule pour les nombres flottants
  * Le nombre de chiffres significatifs pour nombre sous forme exponentielle.
  * Le nombre de digit pour les nombres entiers.
  * Le nombre de caractères pour les chaînes.
  
  Note : Le point est important, il faut le mettre avant le nombre.
* `[type]` indique le type de formatage :
  
  * b ou B pour les nombres binaires.
  * d, u ou i pour les nombres décimaux.
  * o pour les nombres octaux.
  * x ou X pour les nombres hexadécimaux.
  * f pour les nombres flottants.
  * e ou E pour les nombres flottants en notation exponentielle.
  * g ou G pour la notation exponentielle.
  * c pour un caractère
  * p pour inspecter (obj.inspect) un objet
  * s pour les chaînes de caractères.

Exemples : 
```ruby
format('Map%<id>03d.rxdata', id: 5) # "Map005.rxdata"
format('Résultat = %<result> 5.2f', result: 23.256) # "Résultat =  23.26"
```

## Mot de la fin

Une bonne partie des choses au sujet des chaînes de caractères ont été vues, pour comprendre les expressions régulières, je vous laisse regarder la partie Regexp de la documentation de Ruby ou simplement utiliser des sites d'apprentissage des expressions régulières (c'est très bien fait généralement).