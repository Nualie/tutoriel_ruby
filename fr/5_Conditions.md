# Les conditions

En Ruby les conditions fonctionnent d'une manière un peu différente d'autres langages.

Premièrement nous avons deux types de conditions :
* `if` : exécute le code si la condition est valide.
* `unless` exécute le code si la condition n'est pas valide.

La présence de ces deux types de conditions permet une grande flexibilité dans la programmation des conditions (surtout celle des conditions de protection). Plutôt que de tout encadrer dans un gros `not` on utilise simplement `unless` et `unless` sera beaucoup plus efficace que `if not`.

## Qu'est-ce qu'une condition valide ?

Avant de commencer à écrire des conditions, on va se demander qu'est-ce qu'une condition valide.

Nous avons vu qu'il existe des booléens `true` et `false`. Logiquement, `true` valide une condition pendant que `false` ne la valide pas. Mais ce n'est pas tout.

Dans les faits il existe que deux choses qui ne valident pas une condition : `false` et `nil`. Tout le reste, quelque soit sa nature, validera une condition. Ainsi, `0` validera toujours une condition, `[]` aussi, `''` aussi, etc...

Ceci a un effet positif précis : vérifier rapidement que l'objet existe avant de s'en servir. Si vous voulez détecter quand un tableau est vide ou une chaîne est vide, utilisez la méthode `empty?`.

## Les manières d'écrire des conditions

Il existe deux manières d'écrire des conditions en Ruby.
* En bloc :

    ```ruby
    if condition
      # code
    end
    ```
* En fin de ligne
    ```ruby
    code if condition
    ```

Si une condition est écrite en bloc, il faut toujours la terminer par un `end`. Par contre, si une condition est écrite en fin de ligne, il ne faut pas mettre le `end` puisque la partie exécutée par la condition est ce qui se trouve entre le début de ligne et le `if`. (C'est l'assertion avant le `if` pour être plus précis.)

## Les conditions sinon

Quand vous écrivez une condition en bloc, vous pouvez utiliser `else` qui permet d'exécuter le code si la condition n'a pas été validée.

Avec `if` :
```ruby
if condition
  # Code exécuté si condition est valide
else
  # Code exécuté si condition n'est pas valide
end
``` 

Avec `unless` :
```ruby
unless condition
  # Code exécuté si condition n'est pas valide
else
  # Code exécuté si condition est valide
end
```

Il est souvent recommandé d'écrire les conditions `if` plutôt que `unless`. Comme je suis un peu feignant, j'écris la plus facile des deux quand j'y pense. (Comme je suis également un peu bizarre il m'arrive plus souvent de trouver une condition qui ne valide pas facilement).

## Les conditions sinon si

Ces conditions là ne fonctionnent qu'avec le `if` et permettent de décrire plusieurs conditions. Ces conditions sont `elsif`.

Exemple sans else :
```ruby
if condition
  # Code exécuté si condition valide
elsif autre_condition
  # Code exécuté si autre_condition valide
end
```

On peut également en utiliser autant qu'on veut :
```ruby
if condition
  # Code exécuté si condition valide
elsif autre_condition_1
  # Code exécuté si autre_condition_1 valide
elsif autre_condition_2
  # Code exécuté si autre_condition_2 valide
end
```

On peut également terminer nos conditions par un `else` pour couvrir le cas où aucune condition ne valide :
```ruby
if condition
  # Code exécuté si condition valide
elsif autre_condition_1
  # Code exécuté si autre_condition_1 valide
elsif autre_condition_2
  # Code exécuté si autre_condition_2 valide
else
  # Si aucune condition n'a été valide
end
```

Note : Dans un bloc conditionnel, seule une des conditions est exécutée si l'une d'elles est valide, une fois arrivé à la fin du code exécuté, on saute directement au `end`.

## Il est où le switch case ?

En Ruby nous avons un mécanisme similaire, ça génère vite des conditions monstrueusement longues mais c'est là. Ce mécanisme c'est `case` `when` :

```ruby
case valeur
when autre_valeur
  # Partie exécutée si valeur est équivalente à autre valeur
when autre_valeur2, autre_valeur3, autre_valeur4
  # Partie exécutée si valeur est équivalente à l'une des trois autre_valeur renseignées après le when.
else
  # Partie exécutée si valeur ne correspond à rien.
end
```

En Ruby, on ne met pas deux `when` l'un après l'autre pour vérifier plusieurs égalités on sépare toutes les valeurs qui valident par une virgule. En effet, on utilise pas `break` dans un `case` `when`, quand le code a été exécuté on saute directement au `end`.

Exemple :
```ruby
case age
when 0..17 # vérifier 0, 1, 2, 3, 4, ... , 17
  puts 'Tu es mineur.'
when 18
  puts 'Tu es devenu majeur.'
else
  puts 'Tu es majeur.'
end
```

Quand je disais équivalent ça voulait dire que Ruby fait un peu plus qu'une simple vérification d'égalité, quand il y a un Range il teste si la valeur est dans le Range.

## Les conditions ternaires

Ruby comprend aussi les conditions ternaire, elles s'écrivent comme dans tous les autres langages mais suivent les mêmes règles que les autres conditions en Ruby et valident que si la valeur n'est pas `false` ou `nil`.

Exemple :
```ruby
majorite_str = (age >= 18 ? 'majeur' : 'mineur')
puts format('Tu es %<majorite>s.', majorite: majorite_str)
# Affichera si age >= 18 :
# Tu es majeur.
# Sinon :
# Tu es mineur.
```

## Les opérateurs conditionnels

Pour écrire des conditions, il existe plusieurs opérateurs permettant de les décrire.

### L'égalité

L'égalité se vérifie à l'aide de l'opérateur `==`. 

Exemple :
```ruby
5 == 5.0 # true
5 == (5+0i) # true
5 == 5 # true
5 == 6 # false
5 == "5" # false
```
Les premières égalités ont fonctionné car les objets à gauche et à droite étaient de même nature (`Numeric`), la dernière n'a pas fonctionné car la valeur de droite était une chaîne de caractères. Ruby ne convertit pas automatiquement les objets pour vérifier l'égalité.

### L'inégalité

L'inégalité, elle, se vérifie avec l'opérateur `!=`.

Exemple :
```ruby
5 != 6 # true
5 != false # true # objets de nature différente
5 != 5.0 # false # (ils sont égaux)
```

### La vérification

La vérification est ce qui est utilisée dans le `case` `when`. C'est l'opérateur `===`.

Pour l'utiliser, il faut mettre à gauche la valeur vérifiée par le `when` et à droite la valeur renseignée au `case`.

Par exemple :
```ruby
case 5
when Integer
  puts '5 is an integer'
end
```

Est équivalent à :
```ruby
if Integer === 5
  puts '5 is an integer'
end
```

Il faut faire très attention car `left === right` n'est pas du tout équivalent à `right === left`.

### Les opérateurs de comparaison

Il existe 4 opérateurs de comparaison (retournant un booléen).

* `>` : Pour vérifier si la gauche est strictement supérieure à la droite.
* `>=` : Pour vérifier si la gauche est supérieure ou égale à la droite.
* `<` : Pour vérifier si la gauche est strictement inférieure à la droite.
* `<=` : Pour vérifier si la gauche est inférieure ou égale à la droite.

Attention, ces opérateurs ne peuvent pas être utilisés pour tous les types d'objets (un entier ne peut pas être comparé avec une chaîne par exemple).

Exemples :
```ruby
5 < 6 # true
5 > 6 # false
5 < 5 # false
5 <= 5 # true
5 <= 5.0 # true
```

### Les combinaisons de conditions

Si vous voulez écrire plusieurs conditions vous pouvez vous servir de ces deux opérateurs :
* `||` qui décrit un OU (valide la condition si la gauche est vrai, ou la droite est vrai)
* `&&` qui décrit un ET (ne valide pas la condition si la gauche est fausse, ou la droite est fausse)

L'opérateur `||` est prioritaire sur l'opérateur `&&`. Toutefois, si vous désirez expliciter l'ordre de manière plus fine, vous avez le droit d'utiliser les parenthèses.

Une chose importante à noter, ces opérateurs n'exécutent que ce qui est nécessaire à exécuter. Pour `||`, dès qu'il y a une condition validée (à gauche), la droite est complètement ignorée. Pour `&&`, dès qu'il y a une condition invalidée (à gauche) la droite est complètement ignorée.

Exemples :
```ruby
a = nil
b = nil
(a = true) || (b = false) # true
# a = true; b = nil
(a = false) && (b = true) # false
# a = false; b = nil
(a = false) || (b = false) # false
# a = false; b = false
(a = true) && (b = true) # true
# a = true; b = true
```

Je tiens à préciser d'ailleurs que ces opérateurs ne sont pas des opérateurs logiques. Ils réalisent effectivement bien un ET ou un OU selon les tables de vérité, mais le résultat n'est pas nécessairement un booléen.

Exemples :
```ruby
0 && 1 # 1
true && nil # nil
false || 0 # 0
55 || false # 55
```

Ce petit fonctionnement est la raison pour laquelle ces opérateurs peuvent être utilisés pour l'affectation. 

Exemples :
```ruby
a = nil
# a = nil
a ||= 5
# a = 5
a ||= 6
# a = 5
a = nil
# a = nil
a &&= 5
# a = nil
a = 0
# a = 0
a &&= 6
# a = 6
a &&= false
# a = false
a &&= 3
# a = false
```

Exemple concret d'utilisation dans une condition :

```ruby
if heure < 6 || heure >= 22
  puts "C'est la nuit."
else
  puts "C'est la journée."
end
```

### L'inversion

Il existe un opérateur qui permet d'inverser le résultat d'une opération conditionnelle :
`not`.
Il y a un équivalent qui est beaucoup plus utilisé qui est `!`. L'usage de l'un ou l'autre n'a pas le même effet.

* `not` est un opérateur très peu prioritaire, ça veut dire que tout ce qui se trouve après est exécuté avant lui même.
* `!` est un opérateur qui se rapporte à l'élément qui le suit directement (il est équivalent à `-nombre` mais pour des booléens). De ce fait, il est recommandé d'utiliser des parenthèses pour définir ce que vous voulez inverser avec.

Exemples :
```ruby
a = nil
b = nil
not (a = true) && (b = false) # true
# a = true; b = false
a = nil
b = nil
! (a = true) && (b = false) # false
# a = true; b = nil
```

Ici on peut montrer que dans le cas du `not`, l'opérateur `&&` s'est exécuté avant l'opérateur `not` (ce qui fait que b a été affecté et que le résultat final est true). On montre aussi que l'opérateur `!` s'est exécuté avant l'opérateur `&&` vu qu'il se rapporte à l'expression `(a = true)` et non tout le reste. De ce fait, si on suit les règles évoquées précédemment avec l'opérateur `&&`, `!(a = true)` vaut `false` donc `(b = false)` n'est pas exécuté et le résultat est `false`.