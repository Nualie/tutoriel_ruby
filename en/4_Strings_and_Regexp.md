# Strings and Regular Expressions

As mentioned earlier, strings are intended to be used for handling files and communicating with a user. You absolutely must avoid structuring your code around strings (such as using string => value in hashes, for example).

In Ruby, the string type is called String, and regular expressions are Regexp.

A regular expression is defined in two different ways in Ruby:
* `/<expression>/<flags>` is the usual way, where `<expression>` is the regular expression and `<flags>` are the expression's optional flags.
* `%r{<expression>}<flags>` is the alternate way, where `<expression>` is the regular expression (it may contain `/` without the preceding `\`) and `<flags>` are the expression's optional flags.

Regular expressions can help us perform operations on strings. There are certain contexts where we'll be using them a lot.

## Manipulating Strings

We can manipulate strings in a few different ways. Here, we'll be taking a look at the most common and interesting ones.

### Concatenating Strings

Two ways to concatenate strings: 
* `string1 + string2`: concatenates the two strings into one new string.
* `string1 << string2` or `string1.concat(string2)`: concatenates string2 onto string1.

Examples:
```ruby
str1 = 'Hello '
str2 = 'world!'
str1 + str2 # "Hello world!"
str1 << str2 # "Hello world!"
# str1 = "Hello world!"
```

### Concatenating a String Onto the Beginning of Another String

If you want to add content to the beginning of a string, you can use the `prepend` method.

Example:
```ruby
str = 'world!'
str.prepend('Hello ')
# str = "Hello world!"
```

### Changing String Case

It's possible to change the case of a string using these two methods:
* `downcase` : Converts to all lowercase.
* `upcase` : Converts to all uppercase.

### Comparing Strings While Ignoring Case

To do a case-insensitive string comparison, use `casecmp?`.

Example:
```ruby
str = "HeLlO wOrLd!"
str.casecmp?('Hello world!') # true
str == 'Hello world!' # false # Default comparison style
```

### Finding a String At the Beginning or End of a String

If you want to know if a string starts or ends with some other string, you can use two methods:

* `start_with?` whether the string starts with the parameter (which is a string or regular expression).
* `end_with?` whether the string ends with the parameter (a string or regular expression)

Examples:
```ruby
str = 'String to test.'
str.start_with?('String') # true
str.start_with?(/string/i) # true
str.end_with?('.') # true
str.end_with?(/test\./i) # true
```

### Aligning String Contents

There are three ways to align string contents:
* `center` to center the string
* `ljust` to align (justify) the string left
* `rjust` to align (justify) the string right

Examples:
```ruby
str = 'test'
str.center(10) # "   test   "
str.center(10, '=') # "===test==="
str.ljust(10) # "test      "
str.ljust(10, '=') # "test======"
str.rjust(10) # "      test"
str.rjust(10, '=') # "======test"
```

### Cleaning Up a String

There are several methods for cleaning up a string, depending on how you want to do it.

* `chomp`: Deletes the line break at the end of a string (like from user input).
* `strip`: Deletes all "whitespace" characters surrounding the string - spaces, line breaks, tabs,
* `rstrip` : Version of `strip`, but only cleans up the right side of the string.
* `lstrip` : Version of `strip`, but only cleans up the left side of the string.

Examples:
```ruby
str = "   test  \n"
str.chomp # "   test  "
str.strip # "test"
str.lstrip # "test  \n"
str.rstrip # "   test"
```

### Reading Characters From the String

This can be done like with arrays, using the `[]` operator.

Examples:
```ruby
str = 'Hello'
str[1] # "e"
str[1, 2] # "el"
str[1..3] # "ell"
str[2..-1] # "llo"
```

### Modifying Part of a String

Like with arrays, the `[]=` operator lets you partially modify a string in place. It works exactly the same way, except you're changing characters in a string instead of elements in an array.

Example:
```ruby
str = 'H...O'
str[1, 3] = 'ell'
# str = "HellO"
str[-1] = 'o'
# str = "Hello"
```

### Getting a String's Size

Strings have two sizes associated with them:
* `size` is their number of characters
* `bytesize` is their number of bytes

The method you'll want depends on what you're trying to do with the string.

Example:
```ruby
str = "naïve"
str.size # 5
str.bytesize # 6
```

### Reading or Modifying String Bytes

When you care more about the raw bytes of a string than the characters themselves, you can use the two following methods:
* `setbyte` to modify a byte in the string.
* `getbyte` to modify a byte in the string.

Example:
```ruby
str = 'HEllo'
str.getbyte(1) # 69
str.setbyte(1, 101)
# str = "Hello"
```

### Replacing Parts of a String

There are several methods for replacing characters in a string with other characters:
* `tr` replaces instances of characters in the first parameter with the characters in the second parameter.
* `sub` replaces the pattern (first parameter) with the second parameter or the result of the block when executed.
* `gsub` replaces all matches of the pattern (first parameter) by the second parameter or the result of the block when executed.

Examples:
```ruby
str = 'Hello world!'
str.tr('l', '1') # "He11o wor1d!"
str.tr('lo','10') # "He110 w0r1d!"
str.tr('elor', '*') # "H**** w***d!"
str.sub('l', '1') # "He1lo world!"
str.sub(/[a-z]+/, '') # "H world!"
str.sub(/( |!)/) do |full_match|
  next('[space]') if $1 == ' '
  next('(!)')
end # "Hello[space]world!"
str.gsub('l', '1') # "He11o wor1d!"
str.gsub(/[a-z]+/, '') # "H !"
str.gsub(/( |!)/) do |full_match|
  next('[space]') if $1 == ' '
  next('(!)')
end # "Hello[space]world(!)"
```

### Finding Patterns in a String

Two operators can be used to find a pattern in a string, and you can get more precision by using a certain method.

* `=~` Returns the index where the pattern was found, returns `nil` if no match is found.
* `!~` Indicates whether the pattern is not in the string (`true`), returns `false` otherwise.
* `match` Returns all substrings from the string matching the regular expression.

Examples:
```ruby
str = 'Hello world!'
str =~ /e/ # 1
str =~ /z/ # nil
str !~ /z/ # true
str !~ /e/ # false
str.match(/(l|e)+/) # #<MatchData "ell" 1:"l">
```

## Formatting Strings

This is a particularly important part. In Ruby, we can format strings, and there are functions to do this: `sprintf` and `format`. Both are aliases. It is recommended that you use `format` instead of `sprintf` since we tend to refer to the resulting string as "formatted".

The `format` method is a global function. It takes a format string as its first parameter and the rest of the parameters are the replacement strings.

Example:
```ruby
format('You are %<age>d years old.', age: 18) # "You are 18 years old."
```

In this string, `%` marks where we're expecting a value to format, `<age>` indicates the name of that value, and `d` indicates the format of the value (a decimal number).

To generalize this, whenever we want to format some value, we're going to write
`%<[name]>[flag][size][.precision][type]`
where:
* `[name]` must be replaced by the name used to identify the value.
* `[flag]` (optional) can be replaced by:

  * space: replaces every non-digit character with spaces.
  * `0`: replaces all non-digit characters with 0.
  * `#`: Shows 0x / 0b / 0 in front of hexadecimal, binary, and octal numbers respectively.
  * `+`: Shows a + at the beginnings of positive numbers.
  * `-` : Aligns the number to be left-justified instead of right-justified.
* `[size]` (optional) indicates the size (number of characters) you want the formatted value to be.
* `[.precision]` (optional) indicates:

  * For floating point numbers, the number of digits after the decimal
  * For numbers in scientific notation, the number of significant digits
  * For whole numbers, the number of digits.
  * For strings, the number of characters.
  
  Note: The dot is important. You always have to put it before the number.
* `[type]` indicates the formatting type:
  
  * b or B for binary numbers.
  * d, u or i for decimal numbers.
  * o for octal numbers.
  * x or X for hexadecimal numbers.
  * f for floating point numbers.
  * e or E for floating point numbers in scientific notation.
  * g or G for scientific notation.
  * c for a character
  * p to inspect (obj.inspect) an object
  * s for strings.

Examples:
```ruby
format('Map%<id>03d.rxdata', id: 5) # "Map005.rxdata"
format('Result = %<result> 5.2f', result: 23.256) # "Result =  23.26"
```

## Closing Thoughts

We've seen a sizeable portion of what's out there to be used with strings. To get a better understanding of regular expressions, I'll leave it up to you to read the Regexp Ruby documentation page, or you can just use any of the sites dedicated to learning regular expressions (they're often very well-made).