# Classes and Objects
As you may know, in Ruby we say that everything is an object. Imagine how complicated it is make a Ruby tutorial while keeping mentions of objects to a minimum, right up until we start talking about classes...

## Classes

A class in Ruby is an object of type `Class`, and it's used to describe concepts. For example, the `Array` class describes ordered arrays.

A class has a collection of methods which, when called on an object that is an instance of that class, allow us to use and modify it. In Ruby you can't modify an object instance's internals except via calling methods. By default, all methods that you define are public unless you say otherwise.

A public method is a method you can call with the syntax `object.method_name`. Private methods, however, can only be executed by the object's methods.

### Defining a Class

Defining a class is fairly simple. You write the keyword `class`, then write the name of the class using the formatting of a constant. Then you describe the contents, finally ending the definition with the keyword `end`.

```ruby
class Point
  # Defines the concept of a point
end
```

### Defining a Class Constructor

The class constructor in Ruby is the `initialize` method. No matter what, it'll stay private (this is standard practice), so you won't need to specify it as private. `initialize`'s parameters are those that the user will have to pass into the class's singleton `new` method.

```ruby
# Class describing a 2D point
class Point2D
  # Create a new Point2D
  # @param x_pos [Integer] x position of the point
  # @param y_pos [Integer] y position of the point
  def initialize(x_pos, y_pos)
    @x = x_pos
    @y = y_pos
  end
end

point = Point2D.new(5, 6)
# point = #<Point2D:0x03268620 @x=5, @y=6>
```

As you can see, I put some comment lines in the code, but those aren't just any comments. The point of these comments isn't to explain what this basic code is doing to someone who doesn't know anything about Ruby, rather they're for creating auto-generated documentation with [YARD](https://yardoc.org). I highly recommend you follow this practice, it'll simplify your life and will help anyone who will be using your code in the future.

### Defining a Method in a Class

We've already gone over `initialize`, and the principle for other methods is the same. The only difference is the choice whether to make the methods public or private. To do this, use the `private` or `public` keywords.

Example: 
```ruby
class Point2D # No need to document this each time
  
  # Convert an input value to a Integer
  # @param value [Numeric, String]
  # @return [Integer] 
  private def safe_to_i(value)
    return value.to_i if value.is_a?(Numeric) || value.is_a?(String)
    return value.to_s.to_i
  end

  # Assign x to a new value
  # @param x_pos [Integer] new value of x
  def x=(x_pos)
    @x = safe_to_i(x_pos)
  end
end
```

In this example, I've put the keyword `private` right before the `def` keyword on the same line. This tells Ruby that this method should be private.

I didn't put anything there for the `x=` method (which lets us write `point.x = value`) since methods are public by default when defined unless we change the context we're defining them in.

For example:
```ruby
# Some random class
class RandomClass
  private

  # First private method
  def first_private_method
  end

  # Second private method
  def second_private_method
  end

  # First method of RandomClass that is public
  public def first_public_method
  end
end
```

When the `private` or `public` keywords are written on their own line in a class definition, they tell Ruby that everything that follows will be private or public respectively until you specify otherwise and/or change the context.

### Attributes

In Ruby you can define attributes, which are just easy access methods for the variables of an object instance.

There are 3 types of attributes:
* `attr_reader`: Defines a read-only attribute.
* `attr_writer`: Defines a write-only attribute.
* `attr_accessor`: Defines a read/write attribute.

It's recommended to always write attribute modifiers yourself since `attr_writer` and `attr_accessor` break encapsulation. If you write any old thing into an object instance's variables you can end up with bugs.

Example:
```ruby
class Point2D
  # @return [Integer] x coordinate of the point
  attr_reader :x
end
```

When we write `point.x` we'll be reading from the variable `@x` in `Point2D`.

### Defining a Complete Class

Now that we've gone through all this, we're going to define the concept of a 2D point with a few features:
* Should be able to modify coordinates all at once, or just one or the other.
* Should be able to move the point
* Should be able to calculate its distance from another point
* Should be able to copy coordinates from another point

To accomplish this, we're going to use attributes, private methods, and type checking. We're also going to use the keyword `self`, which references back to the object instance.

Check out the result here: [Point2D.rb](../scripts/Point2D.rb).

### Inheritance

Inheritance in object oriented programming is a fairly useful concept. It lets us build upon other concepts without having to rewrite them.

Classes inheriting from a parent class can access all the knowledge of the parent class (methods, constants, etc...).

To specify the parent class, we use the `<` sign after the name of the class, followed by the name of the parent class.
```ruby
class Point3D < Point2D
end
```

Now we're going to build on the concept of a Point 2D to create a Point 3D. 

One thing to keep in mind is that Ruby can only inherit from a single class (no multiple inheritance).

To call the parent class's method when we're redefining it in the child class, we use the keyword `super`. If you don't give it any parameters, it sends the parent method the exact same parameters passed to the child version. This is why it's better to use parentheses when you want to specify the parameters.

Exemples :
```ruby
class Child < Parent
  def method1(param1)
    super # Will pass param1
    # do_something
  end

  def method2(param1)
    super() # Not passing any parameters
    # do_something
  end

  def method3(param1, param2)
    super(param1) # Only passing param1
    # do_something
  end
end
```

I made the [Point3D.rb](../scripts/Point3D.rb) script to show you inheritance.

## Singleton Methods

Singleton methods are methods that belong to one object, and they are not necessarily found in other objects. These methods are defined in a very particular way:

* In another method:

    ```ruby
    def method_name
      def singleton_method
        # do something in the context of the returned object of method_name
      end
      # do stuff
      return an_object
    end
    ```

    The return of `method_name` will give us access to `singleton_method`, which will be a singleton method of the returned object. This means that `singleton_method` has access to the return value as if it had been defined in the class and can be called as such.

* Using the syntax `def obj.method_name`

    This is how we more or less reproduce static functions in Ruby classes:
    ```ruby
    class MyClass
      # Here, self references MyClass
      def self.singleton_method
        # do stuff
      end
    end
    ```
    After this definition we can write `MyClass.singleton_method`.

For classes, there's another syntax for defining these methods:
```ruby
class MyClass
  class << self
    def singleton_method
      # do stuff
    end
  end
end
```
This syntax is useful when you have a lot of methods you want to define.

### Constants

Classes can contain constants, whether they be a subclass or regular constant.

By convention we write constants that aren't classes in all caps and separate words with underscores.

Example:
```ruby
class Point2D
  ORIGIN = new(0, 0).freeze
end
```

Here we've saved the origin of the 2D plane in the `ORIGIN` constant. We've used the `freeze` method to be certain the origin will not be modified (by a call to `set_coordinates` for example). 

If you want to access the `ORIGIN` constant from anywhere outside the Point2D, you'll have to write `Point2D::ORIGIN`. If you are in the class definition you can just write `ORIGIN`.

## Closing Thoughts
I've described a decent number of things relating to classes but we're not done - modules will allow me to introduce Mixins, which we can use to overcome the limitation of no multiple inheritance.