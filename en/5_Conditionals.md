# Conditionals

In Ruby, conditionals work a bit differently from other languages.

First of all, we have two kinds of conditions:
* `if` executes the code if the condition is true.
* `unless` executes the code if the condition isn't true.

Having access to these two styles allows for a lot of flexibility when writing conditionals (especially guard clauses). Rather than enclosing everything in one big `not`, you just need to use `unless` and that will work much nicer than using `if not`.

## What Is a Valid Conditional?

Before we start writing conditionals, we have to think about what a "true condition" even means.

We've seen the two boolean values, `true` and `false`. Logically, `true` constitutes a true condition while `false` does not. But that's not the whole story.

It turns out there are two things that make a condition untrue: `false` and `nil`. Everything else, no matter what it is, counts as a true condition, meaning `0` will always be true along with `[]`, `''`, etc...

This has a nice upside: you can use this behavior to check if an object exists before using it. If you want to check whether an array is empty or not, you can use the `empty?` method.

## Ways to Write Conditionals

There are two ways to write conditionals in Ruby:
* As a block:

    ```ruby
    if condition
      # code
    end
    ```
* At the end of a line:
    ```ruby
    code if condition
    ```

If a conditional is written as a block, it must always end with an `end`. On the other hand, if a conditional is written at the end of a line, you don't need the `end` since the portion of code executed is restricted to between the start of the line and the `if`. (Stated more precisely, the statement is a logical assertion placed before the `if`.)

## If-Else Conditionals

When you write a conditional as a block, you can use `else`, which allows you to execute code if the condition turns out not to be valid.

With `if`:
```ruby
if condition
  # Code executed if condition is true
else
  # Code executed if condition is not true
end
``` 

With `unless`:
```ruby
unless condition
  # Code executed if condition is not true
else
  # Code executed if condition is true
end
```

It's often recommended to write `if` conditionals rather than `unless` conditionals. I'm kind of lazy though, so I tend to use whichever one seems to fit best in the moment. (I'm also kind of a weirdo and end up reasoning my way into writing complex and hard-to-parse conditionals as well).

## If-Elseif Conditionals

These conditionals only work with an initial `if`, and they let you describe multiple conditions in sequence. These are called `elseif`.

Example without else:
```ruby
if condition
  # Code executed if condition is true
elsif other_condition
  # Code executed if other_condition is true
end
```

We can use as many as we want, too:
```ruby
if condition
  # Code executed if condition is true
elsif other_condition_1
  # Code executed if other_condition_1 is true
elsif other_condition_2
  # Code executed if other_condition_2 is true
end
```

Furthermore, we can end our conditionals with an `else` to cover the case where none of the other conditions are true:
```ruby
if condition
  # Code executed if condition is true
elsif autre_condition_1
  # Code executed if other_condition_1 is true
elsif autre_condition_2
  # Code executed if other_condition_2 is true
else
  # If no condition was true
end
```

Note: In a conditional block, only one of the conditions is executed no matter how many would end up being true. Once some code is executed, we jump directly to `end`.

## Where's the Switch Statement?

In Ruby, we do have something similar. It can quickly end up generating monstrously long lists of conditionals, but it's there and it can be used. It's called `case` `when`:

```ruby
case value
when other_value
  # Code executed if value is equivalent to other_value
when other_value2, other_value3, other_value4
  # Code executed if value is equivalent to one of the other_values written after the when.
else
  # Code executed if value doesn't match anything.
end
```

In Ruby, you can't put two `when`s one after the other to check multiple conditions. You have to separate each value to check with a comma. In fact there's not even a `break` statement in a `case` `when`: once some code is executed, it jumps straight to `end`.

Example
```ruby
case age
when 0..17 # check 0, 1, 2, 3, 4, ... , 17
  puts 'You are a minor.'
when 18
  puts 'Happy Birthday! Now you are an adult.'
else
  puts 'You are an adult.'
end
```

When I said it was "similar", I was hinting that Ruby is capable of a bit more than a straightforward equality check: when you use a Range, it will test if the value falls within that Range.

## Ternary Conditionals

Ruby also includes ternary conditionals, which are written the same as in other languages but follow the same rules as other Ruby conditionals. They are true if the value isn't `false` or `nil`.

Example:
```ruby
adulthood_str = (age >= 18 ? 'adult' : 'minor')
puts format('You are a(n) %<adulthood>s.', adulthood: adulthood_str)
# If age >= 18, you'll get:
# You are a(n) adult.
# Otherwise:
# You are a(n) minor.
```

## Conditional Operators

There are several operators that help us define the behavior of conditionals.

### Equality

Equality is checked using the `==` operator. 

Example:
```ruby
5 == 5.0 # true
5 == (5+0i) # true
5 == 5 # true
5 == 6 # false
5 == "5" # false
```
The first equality comparisons worked because the objects on the left and right had the same base type (`Numeric`), the last did not work because the value on the right was a string. Ruby does not automatically convert objects to check whether they are equal.

### Inequality

Inequality uses the `!=` operator.

Example:
```ruby
5 != 6 # true
5 != false # true # objects of a different type
5 != 5.0 # false # (they're equal)
```

### Verification

Verification is what is used in `case` `when` blocks. It is represented by the `===` operator.

To the left of the operator, you place the value that would be checked by the `when`, and to the right you place the value that would be checked by the `case`.

For example:
```ruby
case 5
when Integer
  puts '5 is an integer'
end
```

Is equivalent to:
```ruby
if Integer === 5
  puts '5 is an integer'
end
```

Pay very close attention to what you're doing, because `left === right` is absolutely not equivalent to `right === left`.

### Comparison Operators

There are 4 comparison operators (each returning a boolean).

* `>`: To check if the left side is strictly greater than the right side.
* `>=`: To check if the left side is greater than or equal to the right side.
* `<`: To check if the left side is strictly less than the right side.
* `<=`: To check if the left side is less than or equal to the right side.

Careful, because these operators can't be used for all types of objects (a whole number can't be compared with a string, for example).

Examples:
```ruby
5 < 6 # true
5 > 6 # false
5 < 5 # false
5 <= 5 # true
5 <= 5.0 # true
```

### Combining Conditions

If you want to write multiple conditions you can use these two operators:
* `||` means OR (true if the left side or the right side is true)
* `&&` means AND (is not true if the left or right side is false)

The `||` operator takes precedence over the `&&` operator. However if you want to order them explicitly you can use parentheses.

One important thing to note is that these operators only execute as far as they need to. For `||`, once a single condition is evaluated to true (the left side), the right side is completely ignored. For `&&`, once a single condition is invalidated (the left side), the right is completely ignored. 

Examples:
```ruby
a = nil
b = nil
(a = true) || (b = false) # true
# a = true; b = nil
(a = false) && (b = true) # false
# a = false; b = nil
(a = false) || (b = false) # false
# a = false; b = false
(a = true) && (b = true) # true
# a = true; b = true
```

I also want to make it clear that these operators are not logical operators. They are essentially an implementation of the AND and OR operations from mathematical truth tables, though the results they give are not necessarily boolean types.

Examples:
```ruby
0 && 1 # 1
true && nil # nil
false || 0 # 0
55 || false # 55
```

This small quirk is what allows them to be used for instantiation.

Examples:
```ruby
a = nil
# a = nil
a ||= 5
# a = 5
a ||= 6
# a = 5
a = nil
# a = nil
a &&= 5
# a = nil
a = 0
# a = 0
a &&= 6
# a = 6
a &&= false
# a = false
a &&= 3
# a = false
```

An example of real-life usage of a conditional: 

```ruby
if hour < 6 || hour >= 22
  puts "It's nighttime."
else
  puts "It's daytime."
end
```

### Negation

There is an operator that lets us invert the result of a conditional operation:
`not`.
It has an equivalent that is more often used, `!`. Using one or the other can have different effects.

* `not` is a low precedence operator, meaning whatever comes after it will be executed before the negation is applied.
* `!` is an operator that acts on whatever most directly follows it (it's the equivalent of `-number`, but for booleans). For this reason, it's recommended to always enclose whatever you want to negate in parenthesis when using this operator.

Examples:
```ruby
a = nil
b = nil
not (a = true) && (b = false) # true
# a = true; b = false
a = nil
b = nil
! (a = true) && (b = false) # false
# a = true; b = nil
```

Here, we can show that when `not` is used, the `&&` operator is executed before the `not` operator (which means b is assigned the value of false and the final result is true). You can also see that the `!` operator executed before the `&&` operator, since the nearest thing it can evaluate is the result of `(a = true)`, and not the rest of the conditional. This is why, following the operator precedence rules for `&&`, `!(a = true)` evaluates to `false` and so `(b = false)` is not executed. The final result is then `false`.