# Ordered Arrays
Ordered arrays have the type Array and are objects that let you store multiple objects of any type. In Ruby, they have several uses: 
* Describing a simple list of elements
* Storing dynamic method parameters
* Converting data

## Fundamentals

In the [Variables and Data Types](1_Variables_and_Data_Types.md) section, we saw how to define an array using literals, filling the array immediately with direct values. However, be aware that there are other ways to define an array.

### Creating an Array Dynamically

Sometimes you'll want to define an array that already has some elements in it or that has a fixed size without having to write out all its elements specifically. To do this, we use the `new` method from the Array class. (To understand more on how some of this works, you can reference [Methods and Blocks](6_Methods_and_Blocks.md)).

#### Creating an Array With a Fixed Size

The first parameter of the `new` method in the Array class is the size of the array. If you only give it this parameter, it will create an array containing a number of blank elements (`nil`) equal to the given size.

Example:
```ruby
Array.new(5) # [nil, nil, nil, nil, nil]
```

#### Creating an Array Containing a Specific Element

The second (optional) parameter of the Array class's `new` method lets us specify the element used to fill the array.

Example:
```ruby
Array.new(5, 0) # [0, 0, 0, 0, 0]
```

Careful: When you pass an object as the second parameter, the array is filled with the given object (no copying is done). Because of this, modifying the contents of this object is equivalent to modifying the contents of all objects in the array, since the array is essentially filled with one object, referenced multiple times.

#### Creating an Array Containing Elements of One Type

It's possible to use a block to force the array to be filled with copies of an object.

Example:
```ruby
Array.new(2) { [] } # [[], []]
```

Modifying the first sub-array will not affect the second sub-array.

#### Creating an Array Containing Evaluated Elements

This lets you avoid using a loop right after creating the array, instead allowing you to execute code in a block parameter directly while the array is being created.

Example:
```ruby
Array.new(5) do |index| # Index starts at 0
    number_starting_at_one = index + 1
    next(number_starting_at_one * 2)
end # [2, 4, 6, 8, 10]
```
In this example we've created an array of even, non-null whole numbers.

Note: `next(value)` lets you say explicitly that the value to be written into the array is `value`. Any code on the lines below `next` is not executed since `next` also means "go to the next iteration step".

### Accessing Elements of an Array

As in most programming languages, we can access an element in an array by using the `[]` operator.

Example:
```ruby
array = [3, 6, 8, 9]
array[0] # returns 3
```

#### Accessing Multiple Elements At Once

In Ruby, there's a simple way to access multiple array elements at once: `array[start, size]` or `array[start..end]`.

In both cases, `start` indicates the index where elements will start being returned from. If this is negative, we start counting from the end of the array (-1 = last slot, -2 = second to last slot, etc...).

In that same vein, `end` can be negative like `start`. Only the size must always be positive.

If the index is outside of the array range, you'll get `nil`.

Examples:
```ruby
array = [0, 1, 2, 3, 4, 5]
array[2, 2] # [2, 3]
array[2..4] # [2, 3]
array[-2, 2] # [4, 5]
array[-2, 9999] # [4, 5]
array[2..-1] # [2, 3, 4, 5]
array[-3..-1] # [3, 4, 5]
array[9..10] # nil
array[-20, 1] # nil
```

### Modifying the Elements of an Array

It's possible to modify the elements of an array by using the `[]=` method. This method works the same way as `[]`, except negative indexes outside of the array range cause an `IndexError`.

If the starting index comes after the end of the array, the element is inserted and any extra space is padded with `nil`, starting at the end of the array.

Example:
```ruby
array = [0]
array[3, 2] = [1, 2]
# array = [0, nil, nil, 1, 2]
```

Keep in mind that if the size of the array being inserted does not correspond with the size given, Ruby will delete the missing values in the array receiving the data or will shift the values that are outsize the insertion range.

Examples:
```ruby
array = [1, 2, 3, 4, 5]
array[1, 3] = [1, 2]
# array = [1, 1, 2, 5] # the 4 was deleted
array = [1, 2, 3, 4, 5]
array[1, 2] = [9, 10, 11, 12]
# array = [1, 9, 10, 11, 12, 4, 5] # 4 and 5 were preserved.
```

### Getting an Array's Size

There are absolutely tons of cases where knowing the size of an array is useful. Thankfully this isn't complicated - just call the `size` or `length` methods.

Examples:
```ruby
array = [0, 1, 2]
array.size # 3
array = [0, 1, 2, 9, 3, 22]
array.size # 6
```

### Adding Elements to an Array

There are two methods used to add elements to an array:
* `push` to add them to the end.
* `unshift` to add them to the beginning.

These two methods each have explicit aliases: `append` and `prepend`.

Examples: 
```ruby
array = [0]
array.push(1) # array = [0, 1]
array.push(2, 3) # array = [0, 1, 2, 3]
array.unshift(-1) # array = [-1, 0, 1, 2, 3]
array.unshift(-3, -2) # array = [-3, -2, -1, 0, 1, 2, 3]
```

### Removing Elements From an Array

Similarly, there are methods to remove elements from an array:
* `pop` to remove elements from the end
* `shift` to remove elements from the beginning

We can specify the number of elements to remove.

Example:
```ruby
array = [0, 1, 2, 3, 4, 5, 6, 7]
array.pop # 7
# array = [0, 1, 2, 3, 4, 5, 6]
array.pop(2) # [5, 6]
# array = [0, 1, 2, 3, 4]
array.shift # 0
# array = [1, 2, 3, 4]
array.shift(2) # [1, 2]
# array = [3, 4]
```

### Inserting Elements Into the Middle of an Array

Inserting elements into the middle of an array is easy, just call the `insert` method. The parameters are the index to insert at, and the elements to insert.

Example:
```ruby
array = [0, 1, 2]
array.insert(1, 9) 
# array = [0, 9, 1, 2]
array.insert(2, -2, -3, -4) 
# array = [0, 9, -2, -3, -4, 1, 2]
```

### Deleting Elements From the Middle of an Array

Unlike when inserting elements, when we want to delete them, we have two ways we can go about it.
* Deleting multiple elements with `slice!`.
* Deleting a single element using `delete_at`.

`slice!`'s parameters are the same as `[]` (`slice` is an alias of `[]` but doesn't change the original array, unlike `slice!`, which removes the given portion.)

Examples:
```ruby
array = [0, 1, 2, 3, 4, 5]
array.delete_at(3) # 3
# array = [0, 1, 2, 4, 5]
array.slice!(-3, 2) # [2, 4]
# array = [0, 1, 5]
array.slice!(1..-1) # [1, 5]
# array = [0]
```

### Checking Whether an Element Exists in an Array

By calling the `include?` method, it's possible to check whether an element is in an array. `include?` takes one parameter, the element to check for.

Example:
```ruby
array = [:one, :two, :three]
array.include?(:two) # true
array.include?(:four) # false
```

### Obtaining the Index of an Array Element

There are two methods for this:
* `index` returns the position of the first matching element.
* `rindex` returns the position of the last matching element.

Based on the given criteria (using object equality or comparing object properties), we can pass a parameter or a block.

Note that if the element isn't found, these methods return `nil`.

Examples:
```ruby
array = [0, 5, 8, 0, 7, 4]
array.index(8) # 2
array.index { |element| element.odd? } # 1
array.rindex(0) # 3
array.rindex { |element| element.odd? } # 4
```

### Deleting All Blank Elements in an Array

Sometimes your array contains `nil` values. If you want to easily get rid of these you can use the `compact` method, which creates a new array, or `compact!`, which modifies the array in place.

## Array Iteration

Sometimes you need to explore the contents of an array, and there are three methods to use for this:
* `each`: runs through each array element (block parameter = the element).
* `each_with_index`: same thing, except the index is also included in the block parameter.
* `each_index`: runs through each array index (block parameter = the index).

Examples:
```ruby
array = ['1', 2, 33]
array.each { |element| p element }
# output:
# "1"
# 2
# 33
array.each_with_index { |element, index| puts "array[#{index}] = #{element.inspect}" }
# output:
# array[0] = "1"
# array[1] = 2
# array[2] = 33
array.each_index { |index| p index }
# output:
# 0
# 1
# 2
```

Choose whichever method fits your needs. Take note as well that there are other ways to run through an array if you want to do other things, for example:
* Access all values and modify them in some way: `map` or `collect`.
* Delete all values based on some criteria: `reject` (returns a new array) or `reject!` or `delete_if` (these modify the array).
* Select only the values that meet a given criteria: `select`.
* Find the first element which meets a given criteria: `find`

There really are a whole bunch of them, so I encourage you to read the documentation pages for Enumerable and Array.


## Closing Thoughts

There are tons of seriously useful methods in the Array class, and I simply can't mention all of them. We'll see some of them later, but I highly recommend you take a look at the official Ruby documentation.